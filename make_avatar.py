import py_avataaars as pa
from numpy import random
import sys

file_name = f'{sys.argv[1]}.png'

print(random.choice(list(pa.AvatarStyle)))

avatar = pa.PyAvataaar(
    style=pa.AvatarStyle.CIRCLE,
    skin_color=random.choice(list(pa.SkinColor)),
    hair_color=random.choice(list(pa.HairColor)),
    facial_hair_type=random.choice(list(pa.FacialHairType)),
    facial_hair_color=random.choice(list(pa.HairColor)),
    top_type=random.choice(list(pa.TopType)),
    hat_color=random.choice(list(pa.Color)),
    mouth_type=random.choice(list(pa.MouthType)),
    eye_type=random.choice(list(pa.EyesType)),
    eyebrow_type=random.choice(list(pa.EyebrowType)),
    nose_type=random.choice(list(pa.NoseType)),
    accessories_type=random.choice(list(pa.AccessoriesType)),
    clothe_type=random.choice(list(pa.ClotheType)),
    clothe_color=random.choice(list(pa.Color)),
    clothe_graphic_type=random.choice(list(pa.ClotheGraphicType))
)
avatar.render_png_file(file_name)
drop database if exists contact; 
create database contact; 
use contact; 
create table people(
    id int not null auto_increment primary key,
    name varchar(200) not null
);
create table contact(
    peopleone int,
    peopletwo int,
    FOREIGN KEY (peopleone) REFERENCES people(id),
    FOREIGN KEY (peopletwo) REFERENCES people(id)
);


drop user if exists soleil@'localhost';
create user soleil@'localhost' IDENTIFIED BY 'mdp';
grant all privileges on contact.* to soleil@'localhost';


const mysql = require('mysql2');
const fs = require('fs');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'soleil',
  password: 'secret',
  database: 'contact'
});

connection.connect((err) => {
    if (err) throw err;
    console.log('Connecté à la base de données MySQL!');
  
    connection.query('SELECT * FROM people', (err, people) => {
        if (err) throw err;
        console.log('Données récupérées avec succès!'); 

        connection.query('SELECT * FROM contact', (err, contact) => {
            let data = {
                people: people,
                contact: contact
            }

            data = JSON.stringify(data);

            fs.writeFile('contacts.json', data, (err) => {
                if (err) throw err;
                console.log('Données écrites avec succès dans le fichier "contacts.json"!');
    
                connection.end((err) => {
                if (err) throw err;
                console.log('Connexion à la base de données MySQL terminée!');
                });
            });
        })
  });
});